### Installation

Add in repositories to your composer.json

```sh
"repositories": {
        "nas-service": {
            "type": "vcs",
            "url": "https://gitlab.com/nas-service/client.git"
        }
}
```

Then run

```sh
composer require nas-service/client
```

Create config file in path/to/root/config/nas_service_client.php

nas_service_client.php file
```php
<?php
return [
    'environment' => 'dev',
    'host' => 'hrzn.io'
];
```


### Development

Exmaple using

```php
<?php
  use NasService\NasServiceClient;
  
  $client = (new NasServiceClient())->getClient();
```
