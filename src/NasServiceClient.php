<?php

namespace NasService;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NasServiceClient
{
    private string $baseUrl;

    private HttpClientInterface $client;
    private EventDispatcherInterface $eventDispatcher;
    private RequestStack $requestStack;
    private ParameterBagInterface $parameterBag;


    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        RequestStack             $requestStack,
        ParameterBagInterface    $parameterBag)
    {

        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
        $this->baseUrl = $this->parameterBag->get('nas_service_api_base_url');
        $this->init();
    }

    public function init(string $baseUrl = null)
    {
        $request = $this->requestStack->getCurrentRequest();
        $params = [];
        if (!is_null($request)) {
            $params['headers']['User-Agent'] = $request->headers->get('User-Agent', 'HttpClient');
            $params['headers']['X-Forwarded-For'] = $request->getClientIp();
            $params['headers']['Origin'] = $request->getSchemeAndHttpHost();
        }
        if ($baseUrl) {
            $this->client = HttpClient::createForBaseUri($baseUrl, $params);
        } else {
            $this->client = HttpClient::createForBaseUri($this->baseUrl, $params);
        }
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getClient(): HttpClientInterface
    {
        return $this->client;
    }

    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    public function setBaseUrl(string $baseUrl)
    {
        $this->init($baseUrl);
    }

}
